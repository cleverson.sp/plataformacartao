package br.com.Imersao.PlataformaCartao.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.Imersao.PlataformaCartao.models.Cartao;
import br.com.Imersao.PlataformaCartao.models.Compra;
import br.com.Imersao.PlataformaCartao.repositories.CompraRepository;

@Service
public class CompraService {

	@Autowired
	private CompraRepository compraRepository;
	@Autowired
	private CartaoService cartaoService;
	@Autowired
	private ClienteService clienteService;

	public Compra realizarLancamento(Compra compra) {

		cartaoService.consultaCartao(compra.getCartao().getNumCartao());
		clienteService.consultaCliente(compra.getCliente().getIdCliente());

		return compraRepository.save(compra);
	}

	public Iterable<Compra> extratoCompras(Cartao cartao) {

		Iterable<Compra> listaCompras = compraRepository.findAllByCartao(cartao);

		return listaCompras;
	}

	public Double saldoFatura(Cartao cartao) {
		Iterable<Compra> compras = compraRepository.findAllByCartao(cartao);
		Double valorFatura = 0.0;

		for (Compra compra : compras) {
			valorFatura += compra.getValorCompra();
		}

		return valorFatura;
	}

	public Compra estornoCompra(Compra idCompra) {

		compraRepository.delete(idCompra);

		return idCompra;
	}

}
