package br.com.Imersao.PlataformaCartao.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.Imersao.PlataformaCartao.models.Cartao;
import br.com.Imersao.PlataformaCartao.repositories.CartaoRepository;

@Service
public class CartaoService {

	@Autowired
	private CartaoRepository cartaoRepository;

	public Cartao consultaCartao(Long numCartao) {
		Optional<Cartao> optional = cartaoRepository.findAllBynumCartao(numCartao);

		if (optional.isPresent()) {
			return optional.get();
		}
		throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "cartao nao encontrado");
	}

	public Cartao ativarCartao(Cartao cartao) {
		Cartao statusCartao = consultaCartao(cartao.getNumCartao());

		if (statusCartao.isIndicadorBloqueio() == true) {
			System.out.println("Service Indicador bloqueio ---->" + statusCartao.isIndicadorBloqueio());
			statusCartao.setIndicadorBloqueio(false);
			return cartaoRepository.save(statusCartao);

		}

		throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cartao já desbloqueado");

	}

	public Cartao bloquearCartao(Cartao cartao) {
		Cartao statusCartao = consultaCartao(cartao.getNumCartao());

		if (statusCartao.isIndicadorBloqueio() == false) {
			System.out.println("Service Indicador bloqueio ---->" + statusCartao.isIndicadorBloqueio());
			statusCartao.setIndicadorBloqueio(true);
			return cartaoRepository.save(statusCartao);

		}

		throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cartao já bloqueado");

	}


}
