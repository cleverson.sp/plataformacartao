package br.com.Imersao.PlataformaCartao.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.Imersao.PlataformaCartao.enums.EnumBandeira;
import br.com.Imersao.PlataformaCartao.models.Cliente;
import br.com.Imersao.PlataformaCartao.repositories.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	private CartaoService cartaoService;

	private EnumBandeira enumBandeira;

	public Cliente incluirCliente(Cliente cliente) {

		if (enumBandeira.VISA.toString().equals(cliente.getCartao().getBandeira().getNomeBandeira().toUpperCase())
				|| enumBandeira.MASTERCARD.toString()
						.equals(cliente.getCartao().getBandeira().getNomeBandeira().toUpperCase())
				|| enumBandeira.ELO.toString().equals(cliente.getCartao().getBandeira().getNomeBandeira().toUpperCase())
				|| enumBandeira.MASTERCARD.toString()
						.equals(cliente.getCartao().getBandeira().getNomeBandeira().toUpperCase())
				|| enumBandeira.HIPERCARD.toString()
						.equals(cliente.getCartao().getBandeira().getNomeBandeira().toUpperCase())) {

			return clienteRepository.save(cliente);
		}

		throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bandeira inválida");

	}

	// corrigir esse !=
	public Cliente alterarDadosCliente(Cliente cliente) {

		Cliente listaCliente = consultaCliente(cliente.getIdCliente());

		if (cliente.getNome() != null) {
			System.out.println("Service nome ---->" + cliente.getNome());
			listaCliente.setNome(cliente.getNome());
		}

		if (cliente.getEndereco() != null) {
			System.out.println("Service endereco ---->" + cliente.getEndereco());
			listaCliente.setEndereco(cliente.getEndereco());
		}

		if (cliente.getTelefone() != null) {
			System.out.println("service telefone ---->" + cliente.getTelefone());
			listaCliente.setTelefone(cliente.getTelefone());
		}

		return clienteRepository.save(listaCliente);
	}

	public Cliente consultaCliente(int idCliente) {
		Optional<Cliente> optional = clienteRepository.findById(idCliente);
		if (optional.isPresent()) {
			return optional.get();
		}
		throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cliente nao encontrado");
	}

}
