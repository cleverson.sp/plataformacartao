package br.com.Imersao.PlataformaCartao.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.Imersao.PlataformaCartao.models.Cartao;
import br.com.Imersao.PlataformaCartao.models.Cliente;
import br.com.Imersao.PlataformaCartao.models.Compra;
import br.com.Imersao.PlataformaCartao.models.Fatura;

@Service
public class FaturaService {

	@Autowired
	private CompraService compraService;
	
	@Autowired
	private ClienteService clienteService;
	
	

	public Fatura consolidaFatura(Cartao cartao, Cliente cliente) {
		
		Fatura fatura = new Fatura();
		Double saldo = 0.0;
		
		Iterable<Compra> listaCompras = compraService.extratoCompras(cartao);
		saldo = compraService.saldoFatura(cartao);
		
		Cliente dadosCliente = clienteService.consultaCliente(cliente.getIdCliente());

		fatura.setCliente(dadosCliente);
		fatura.setListaFatura(listaCompras);
		fatura.setTotalFatura(saldo);

		return fatura;
	}

}
