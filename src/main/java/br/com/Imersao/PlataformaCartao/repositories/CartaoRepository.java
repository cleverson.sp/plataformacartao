package br.com.Imersao.PlataformaCartao.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.Imersao.PlataformaCartao.models.Cartao;

public interface CartaoRepository extends CrudRepository<Cartao, Long>{
	
	public Optional <Cartao> findAllBynumCartao(Long numCartao);

}
