package br.com.Imersao.PlataformaCartao.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.Imersao.PlataformaCartao.models.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
	

}
