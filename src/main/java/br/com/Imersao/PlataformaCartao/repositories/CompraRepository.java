package br.com.Imersao.PlataformaCartao.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.Imersao.PlataformaCartao.models.Cartao;
import br.com.Imersao.PlataformaCartao.models.Compra;

public interface CompraRepository extends CrudRepository<Compra, Integer>{
	
	public Iterable<Compra> findAllByCartao(Cartao cartao);
}
