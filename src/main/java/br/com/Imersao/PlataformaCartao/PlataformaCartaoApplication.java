package br.com.Imersao.PlataformaCartao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlataformaCartaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlataformaCartaoApplication.class, args);
	}

}
