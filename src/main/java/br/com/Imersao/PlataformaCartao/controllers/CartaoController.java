package br.com.Imersao.PlataformaCartao.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.Imersao.PlataformaCartao.models.Cartao;
import br.com.Imersao.PlataformaCartao.services.CartaoService;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

	@Autowired
	private CartaoService cartaoService;

	@PostMapping("/ativar/{cartao}")
	public Cartao ativarCartao(@PathVariable Cartao cartao) {
		return cartaoService.ativarCartao(cartao);
	}
	
	@PostMapping("/bloquear/{cartao}")
	public Cartao bloquearCartao(@PathVariable Cartao cartao) {
		return cartaoService.bloquearCartao(cartao);
	}
	
//	@GetMapping("/geracartao")
//	public long geraCartao() {
//		
//		return cartaoService.geraCartao();
//	}

}
