package br.com.Imersao.PlataformaCartao.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.Imersao.PlataformaCartao.models.Cartao;
import br.com.Imersao.PlataformaCartao.models.Compra;
import br.com.Imersao.PlataformaCartao.services.CartaoService;
import br.com.Imersao.PlataformaCartao.services.CompraService;

@RestController
@RequestMapping("/lancamento")
public class ComprasController {

	@Autowired
	private CartaoService cartaoService;

	@Autowired
	private CompraService compraService;

	@GetMapping("/validar/{numCartao}")
	public Cartao consultaCartao(@PathVariable Long numCartao) {
		return cartaoService.consultaCartao(numCartao);
	}

	@PostMapping("/efetiva")
	public Compra realizarLancamento(@RequestBody Compra compra) {
		return compraService.realizarLancamento(compra);
	}

	@GetMapping("/compras/{numCartao}")
	public Iterable<Compra> extratoCompras(@PathVariable  Cartao numCartao) {
		return compraService.extratoCompras(numCartao);
	}

	@GetMapping("/saldo/{cartao}")
	public Double saldoFatura(@PathVariable Cartao cartao) {
		return compraService.saldoFatura(cartao);
	}
	
	@DeleteMapping("/estorno/{idCompra}")
	public Compra estornoCompra(Compra idCompra) {
		return compraService.estornoCompra(idCompra);
	}

}
