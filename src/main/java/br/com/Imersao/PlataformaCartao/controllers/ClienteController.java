package br.com.Imersao.PlataformaCartao.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.Imersao.PlataformaCartao.models.Cliente;
import br.com.Imersao.PlataformaCartao.services.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@PostMapping("/cadastro")
	public Cliente incluirCliente(@RequestBody Cliente cliente) {
		return clienteService.incluirCliente(cliente);
	}

	@PostMapping("/altera")
	public Cliente alterarDadosCliente(@RequestBody Cliente cliente) {
		return clienteService.alterarDadosCliente(cliente);
	}
	
	@GetMapping("/consulta/{cliente}")
	public Cliente consultaCliente(@PathVariable Cliente cliente) {
		return clienteService.consultaCliente(cliente.getIdCliente());
	}


}
