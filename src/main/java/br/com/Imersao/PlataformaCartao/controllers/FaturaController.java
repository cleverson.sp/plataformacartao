package br.com.Imersao.PlataformaCartao.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.Imersao.PlataformaCartao.models.Cartao;
import br.com.Imersao.PlataformaCartao.models.Cliente;
import br.com.Imersao.PlataformaCartao.models.Fatura;
import br.com.Imersao.PlataformaCartao.services.FaturaService;

@RestController
@RequestMapping("/fatura")
public class FaturaController {

	@Autowired
	private FaturaService faturaService;

	
	@GetMapping("/extrato")
	public Fatura extrato(@RequestParam Cartao cartao, @RequestParam Cliente cliente) {
		System.out.println("Controller --->"+cartao.getNumCartao());
		System.out.println("Controller --->"+cliente.getIdCliente());
		return faturaService.consolidaFatura(cartao, cliente);

	}

}
