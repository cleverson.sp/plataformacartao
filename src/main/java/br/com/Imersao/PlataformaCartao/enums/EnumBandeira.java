package br.com.Imersao.PlataformaCartao.enums;

public enum EnumBandeira {
	VISA(1),
	MASTERCARD(2),
	ELO(3),
	HIPERCARD(4);
	
	private int idMarcaBandeira;
	
	
	EnumBandeira(int idMarcaBandeira){
		this.idMarcaBandeira = idMarcaBandeira;
	}

	public int getidMarcaBandeira() {
		return idMarcaBandeira;
	}
	
	
}
