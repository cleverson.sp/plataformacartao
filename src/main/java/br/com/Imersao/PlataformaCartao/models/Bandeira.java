package br.com.Imersao.PlataformaCartao.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Bandeira {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idBandeira;
	private String nomeBandeira;

	public String getNomeBandeira() {
		return nomeBandeira;
	}

	public int getIdBandeira() {
		return idBandeira;
	}

	public void setIdBandeira(int idBandeira) {
		this.idBandeira = idBandeira;
	}

	public void setNomeBandeira(String nomeBandeira) {
		this.nomeBandeira = nomeBandeira;
	}

}
