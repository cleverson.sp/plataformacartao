package br.com.Imersao.PlataformaCartao.models;

public class Fatura {
	


	private Iterable<Compra> listaFatura;
	private Double totalFatura;
	private String cliente;
	

	public Iterable<Compra> getListaFatura() {
		return listaFatura;
	}
	public void setListaFatura(Iterable<Compra> listaCompras) {
		this.listaFatura = listaCompras;
	}
	public Double getTotalFatura() {
		return totalFatura;
	}
	public void setTotalFatura(Double totalFatura) {
		this.totalFatura = totalFatura;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente.getNome();
	}


}
