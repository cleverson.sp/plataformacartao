package br.com.Imersao.PlataformaCartao.models;

import java.util.Random;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Cartao {

	@Id
	private long numCartao;

	@OneToOne(cascade = CascadeType.ALL)
	private Bandeira bandeira;

	private boolean indicadorBloqueio = true;

	public long getNumCartao() {
		numCartao = geraCartao();
		return numCartao;
	}

	public void setNumCartao(long numCartao) {
		this.numCartao = numCartao;
	}

	public Bandeira getBandeira() {
		return bandeira;
	}

	public void setBandeira(Bandeira bandeira) {
		this.bandeira = bandeira;
	}

	public boolean isIndicadorBloqueio() {
		return indicadorBloqueio;
	}

	public boolean setIndicadorBloqueio(boolean indicadorBloqueio) {
		return this.indicadorBloqueio = indicadorBloqueio;
	}

	public long geraCartao() {

		Random gerador = new Random();

		long numero = 1000000000 + gerador.nextLong();

		if (numero < 0) {
			numero = numero * (-1);
		}

		return numero;
	}

}
